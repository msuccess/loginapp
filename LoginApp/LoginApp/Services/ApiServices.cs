﻿using LoginApp.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace LoginApp.Services
{
    public class ApiServices
    {
        public string UrlBase = "http://localhost:50277/";

        public HttpClient Client;

        public ApiServices(HttpClient client)
        {
            Client = client;
        }

        public async Task<bool> RegisterUserAsync(UserDto userDto)
        {
            var user = new User
            {
                Username = userDto.Username,
                Password = userDto.Password,
                ConfirmPassword = userDto.ConfirmPassword
            };
            var json = JsonConvert.SerializeObject(user);

            HttpContent httpContent = new StringContent(json);
            httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var response = await Client.PostAsync("http://localhost:50277/api/Register", httpContent);

            return response.IsSuccessStatusCode;
        }

        public async Task<string> LoginAsync(LoginDto loginDto)
        {
            var keyValue = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("username", loginDto.Username),
                new KeyValuePair<string, string>("password", loginDto.Password),
                new KeyValuePair<string, string>("grant_type", "password")
            };

            var request = new HttpRequestMessage(HttpMethod.Post, "http://localhost:50277/api/Register" + "Token");

            request.Content = new FormUrlEncodedContent(keyValue);

            var response = await Client.SendAsync(request);

            var content = await response.Content.ReadAsStringAsync();

            JObject jwtJObject = JsonConvert.DeserializeObject<dynamic>(content);

            var accessTokenExpiration = jwtJObject.Values<DateTime>(".expires");

            var accessToken = jwtJObject.Value<string>("access_token");

            return accessToken;
        }

        public async Task<List<User>> GetUsersAsync(string accessToken)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(
                "Bearer", accessToken);

            var json = await client.GetStringAsync("http://localhost:50277/api/Register/api/ideas");

            var users = JsonConvert.DeserializeObject<List<User>>(json);

            return users;
        }
    }
}