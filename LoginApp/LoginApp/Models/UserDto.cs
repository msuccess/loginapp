﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LoginApp.Models
{
    public class UserDto
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }

        public string Message { get; set; }

    }
}
