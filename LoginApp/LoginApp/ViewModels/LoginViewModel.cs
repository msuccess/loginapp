﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Android.Text;
using LoginApp.Models;
using LoginApp.Services;
using Xamarin.Forms;

namespace LoginApp.ViewModels
{
    public class LoginViewModel
    {
        private readonly ApiServices _apiServices;
        private readonly LoginDto _loginDto;
        public LoginViewModel(ApiServices apiServices, LoginDto loginDto)
        {
            _apiServices = apiServices;
           _loginDto = loginDto;
        }

       
        public ICommand LoginCommand
        {
            get
            {
                return new Command(async () =>
                {
                    await _apiServices.LoginAsync(_loginDto);
                });
            }
        }
    }
}
