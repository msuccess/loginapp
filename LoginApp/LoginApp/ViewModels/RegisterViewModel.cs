﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Android.App;
using LoginApp.Models;
using LoginApp.Services;
using Xamarin.Forms;

namespace LoginApp.ViewModels
{
   public class RegisterViewModel
    {
        private readonly ApiServices _apiServices ;
        private readonly UserDto _userDto;
        public RegisterViewModel(ApiServices apiServices, UserDto userDto)
        {
            _apiServices = apiServices;
            _userDto = userDto;
        }

        public ICommand RegisterCommand
        {
            get
            {
                return new Command(async () =>
                {
                    var isRegister = await _apiServices.RegisterUserAsync(_userDto);


                    _userDto.Message = isRegister ? "Success" : "Please try again :(";
                });
            }
        }
    }
}
